<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- The above 3 meta tags *must* come first in the head -->

    <!-- SITE TITLE -->
    <title>Schedule | Intelligent Transport System Indonesia 2019</title>
    <meta name="description" content="Responsive EventHunt HTML Template"/>
    <meta name="keywords" content="Bootstrap3, Event,  Conference, Meetup, Template, Responsive, HTML5"/>
    <meta name="author" content="themearth.com"/>

    <!-- twitter card starts from here, if you don't need remove this section -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@yourtwitterusername"/>
    <meta name="twitter:creator" content="@yourtwitterusername"/>
    <meta name="twitter:url" content="http://yourdomain.com"/>
    <meta name="twitter:title" content="Your home page title, max 140 char"/>
    <!-- maximum 140 char -->
    <meta name="twitter:description" content="Your site description, maximum 140 char "/>
    <!-- maximum 140 char -->
    <meta name="twitter:image" content="assets/img/twittercardimg/twittercard-280-150.jpg"/>
    <!-- when you post this page url in twitter , this image will be shown -->
    <!-- twitter card ends from here -->

    <!-- facebook open graph starts from here, if you don't need then delete open graph related  -->
    <meta property="og:title" content="Your home page title"/>
    <meta property="og:url" content="http://your domain here.com"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:site_name" content="Your site name here"/>
    <!--meta property="fb:admins" content="" /-->  <!-- use this if you have  -->
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="assets/img/opengraph/fbphoto.jpg"/>
    <!-- when you post this page url in facebook , this image will be shown -->
    <!-- facebook open graph ends from here -->

    <!--  FAVICON AND TOUCH ICONS -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/img/favicon/manifest.json">

    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="assets/libs/fontawesome/css/font-awesome.min.css" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="assets/libs/maginificpopup/magnific-popup.css" media="all"/>

    <!-- Time Circle -->
    <link rel="stylesheet" href="assets/libs/timer/TimeCircles.css" media="all"/>

    <!-- OWL CAROUSEL CSS -->
    <link rel="stylesheet" href="assets/libs/owlcarousel/owl.carousel.min.css" media="all" />
    <link rel="stylesheet" href="assets/libs/owlcarousel/owl.theme.default.min.css" media="all" />

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Oswald:400,700%7cRaleway:300,400,400i,500,600,700,900"/>

    <!-- MASTER  STYLESHEET  -->
    <link id="lgx-master-style" rel="stylesheet" href="assets/css/style-default.min.css" media="all"/>

    <!-- MODERNIZER CSS  -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
	
	<!-- CUSTOM  STYLESHEET  -->
    <link rel="stylesheet" href="assets/css/style-custom.css" media="all"/>
</head>

<body class="home">

<?php 
    // persiapkan curl
    $ch = curl_init(); 

    // set url 
    curl_setopt($ch, CURLOPT_URL, "http://localhost:81/api/event");

    // return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

    // $output contains the output string 
    $output = curl_exec($ch); 

    // tutup curl 
    curl_close($ch);      

    // menampilkan hasil curl
    $result = json_decode($output,true);
?>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="lgx-container ">
<!-- ***  ADD YOUR SITE CONTENT HERE *** -->


<!--HEADER-->
<header>
    <div id="lgx-header" class="lgx-header">
        <div class="lgx-header-position lgx-header-position-white lgx-header-position-fixed "> <!--lgx-header-position-fixed lgx-header-position-white lgx-header-fixed-container lgx-header-fixed-container-gap lgx-header-position-white-->
            <div class="lgx-container"> <!--lgx-container-fluid-->
                <nav class="navbar navbar-default lgx-navbar">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="lgx-logo">
                            <a href="index.html" class="lgx-scroll">
                                <img src="assets/img/logo-its-white.png" alt="Eventhunt Logo"/>
                            </a>
                        </div>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <div class="lgx-nav-right navbar-right">
                            <div class="lgx-cart-area">
                                <a class="lgx-btn lgx-btn-red" href="#">Buy Ticket</a>
								<a class="lgx-btn" href="#">Check Payment</a>
                            </div>
                        </div>
                        <ul class="nav navbar-nav lgx-nav navbar-right">
                            
                            <li><a class="lgx-scroll" href="#">Home</a></li>
                            <li><a class="lgx-scroll" href="#">About</a></li>
							<!--<li><a class="lgx-scroll" href="#lgx-speakers">Speaker</a></li>-->
                            <li><a class="lgx-scroll" href="index.html">Schedule</a></li>
                            <li><a class="lgx-scroll" href="#">Contact</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </nav>
            </div>
            <!-- //.CONTAINER -->
        </div>
    </div>
</header>
<!--HEADER END-->



    <!--Banner Inner-->
    <section>
        <div class="lgx-banner lgx-banner-inner">
            <div class="lgx-page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading-area">
                                <div class="lgx-heading lgx-heading-white">
                                    <h2 class="heading">Event Schedule</h2>
                                </div>
                                <ul class="breadcrumb">
                                    <li><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                                    <li class="active">Schedule</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.Banner Inner-->


    <main>
        <div class="lgx-page-wrapper">
            <!--SCHEDULE-->
            <section>
                <div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="lgx-heading">
								<h2 class="heading">Event Schedule</h2>
								<h3 class="subheading">Welcome to the dedicated to building remarkable Schedule!</h3>
							</div>
						</div>
					</div>
                    <div class="row">
						<div class="col-xs-12">
							<div class="lgx-tab lgx-tab2"> <!--lgx-tab2 lgx-tab-vertical-->
								<ul class="nav nav-pills lgx-nav lgx-nav-nogap lgx-nav-colorful">  <!--lgx-nav-nogap lgx-nav-colorful-->
									<li class="active"><a data-toggle="pill" href="#home"><h3>DAY <span>1</span></h3> <p><span>26 </span>Oct, 2019</p></a></li>
									<!--<li><a data-toggle="pill" href="#menu1"><h3>DAY <span>2</span></h3> <p><span>27 </span>Oct, 2019</p></a></li>
									<li><a data-toggle="pill" href="#menu2"><h3>DAY <span>3</span></h3> <p><span>28 </span>Oct, 2019</p></a></li>-->
								</ul>
								<div class="tab-content lgx-tab-content">


									<div id="home" class="tab-pane fade in active">

										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
										<?php foreach($result['rows']  as $key=>$val) { ?>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading-<?=$val['id'];?>">
													<div class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$val['id'];?>" aria-expanded="true" aria-controls="collapse<?=$val['id'];?>">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-1.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time"><?=$val['start_time'];?> - <?=$val['end_time'];?></h4>
																	<h4 class="title"><?=$val['title'];?></h4>
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse<?=$val['id'];?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading-<?=$val['id'];?>">
													<div class="panel-body">
														<p class="text">
															<?=$val['detail'];?>
														</p>
														<h4 class="location"><strong>Location:</strong>  <?=$val['location'];?></h4>
													</div>
												</div>
											</div>
											<?php } ?>
											

									</div>


									<div id="menu1" class="tab-pane fade">

										<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading13">
													<div class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse13" aria-expanded="true" aria-controls="collapse13">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d3-1.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">07:00 <span>Am</span> - 09:00 <span>Am</span></h4>
																	<h4 class="title">Breakfast at Hotel</h4>
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse13" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading13">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading14">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse14" aria-expanded="true" aria-controls="collapse14">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-2.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">09:00 <span>Am</span> - 09:30 <span>Am</span></h4>
																	<h3 class="title">Goes to Jasa Marga Bali Office via TOL Bali Mandara</h3>
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Pick up point at Door 2 Arrival T3 SHIA </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading15">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse15" aria-expanded="true" aria-controls="collapsefour">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-3.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">09:30 <span>Am</span> - 11:30 <span>Am</span></h4>
																	<h3 class="title">Visit Jasa Marga Bali Command Center Room</h3>
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Soekarno Hatta Airport to BNI City Station </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading16">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse16" aria-expanded="true" aria-controls="collapsefive">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-4.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">11:30 <span>Am</span> - 12:00 <span>Am</span></h4>
																	<h3 class="title">Goes to Mang Engking Resto</h3>
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse16" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  BNI City Station</h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading17">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse17" aria-expanded="true" aria-controls="collapsefive">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-5.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">12:00 <span>Am</span> - 13.30 <span>Am</span></h4>
																	<h3 class="title">Lunch at Mang Engking Resto</h3>
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading18">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse18" aria-expanded="true" aria-controls="collapsefive">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-6.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">13:30 <span>Am</span> - 14:30 <span>Pm</span></h4>
																	<h3 class="title">Goes to GWK</h3>
																</div>
																
															</div>
														</a>
													</div>
												</div>
												<div id="collapse18" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
													<div class="panel-body">
														<p class="text">
															Note: We serve vegetarian menu in all dine
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading19">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse19" aria-expanded="true" aria-controls="collapsefive">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-7.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">14:30 <span>Am</span> - 16:30 <span>Pm</span></h4>
																	<h3 class="title">Garuda Wisnu Kencana Cultural Park (GWK)</h3>
																</div>
																
															</div>
														</a>
													</div>
												</div>
												<div id="collapse19" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading20">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse20" aria-expanded="true" aria-controls="collapsefive">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-6.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">16:30 <span>Am</span> - 17:00 <span>Pm</span></h4>
																	<h3 class="title">Goes to Pura Uluwatu</h3>
																</div>
																
															</div>
														</a>
													</div>
												</div>
												<div id="collapse20" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading21">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse21" aria-expanded="true" aria-controls="collapsefive">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-9.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">17:00 <span>Am</span> - 19:30 <span>Pm</span></h4>
																	<h3 class="title">Watching perform Tari Kecak (Kecak Dance)</h3>
																</div>
																
															</div>
														</a>
													</div>
												</div>
												<div id="collapse21" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading21">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Pura Uluwatu, <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading22">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse22" aria-expanded="true" aria-controls="collapsefive">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-1.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">19:30 <span>Am</span> - 20:30 <span>Pm</span></h4>
																	<h3 class="title">Goes to Jimbaran</h3>
																</div>
																
															</div>
														</a>
													</div>
												</div>
												<div id="collapse22" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading22">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading23">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse23" aria-expanded="true" aria-controls="collapsefive">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-11.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">20:30 <span>Am</span> - 22:00 <span>Pm</span></h4>
																	<h3 class="title">Dinner at Jimbaran Resto</h3>
																</div>
																
															</div>
														</a>
													</div>
												</div>
												<div id="collapse23" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading23">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading24">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse24" aria-expanded="true" aria-controls="collapsefive">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-12.png" alt="Speaker"/>
																</div>
																
																<div class="schedule-info">
																	<h4 class="time">22:00 <span>Am</span> - 22:15 <span>Pm</span></h4>
																	<h3 class="title">Goes back to Hotel</h3>
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse24" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading24">
													<div class="panel-body">
														<p class="text">
															Dinner Provided at Hotel Resto. Note: We serve vegetarian menu in all dine
														</p>
														<h4 class="location"><strong>Location:</strong>  InterContinental Bali Resort</h4>
													</div>
												</div>
											</div>
										</div>

									</div>


									<div id="menu2" class="tab-pane fade">

										<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading31">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse31" aria-expanded="true" aria-controls="collapseOne3">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d3-1.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">08:00 <span>Am</span> - 09:00 <span>Am</span></h4>
																	<h3 class="title">Breakfast at Hotel</h3>
																	
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse31" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading31">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading32">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse32" aria-expanded="true" aria-controls="collapseTwo3">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-2.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">09:00 <span>Am</span> - 10:00 <span>Am</span></h4>
																	<h3 class="title">Checkout</h3>
																	
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse32" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading32">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading33">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse33" aria-expanded="true" aria-controls="collapseThree3">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d3-3.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">10:00 <span>Am</span> - 10:30 <span>Am</span></h4>
																	<h3 class="title">Goes to BPPTD Bali</h3>
																	
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse33" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading33">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading34">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse34" aria-expanded="true" aria-controls="collapseThree3">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d3-3.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">10:30 <span>Am</span> - 11:00 <span>Am</span></h4>
																	<h3 class="title">Watching perform Drumband by Taruna BPPTD</h3>
																	
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse34" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading34">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading35">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse35" aria-expanded="true" aria-controls="collapseThree3">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d3-4.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">11:00 <span>Am</span> - 12:00 <span>Am</span></h4>
																	<h3 class="title">Goes to Bebek Tepi Sawah Resto</h3>
																	
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse35" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading35">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading36">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse36" aria-expanded="true" aria-controls="collapseThree3">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d3-4.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">12:00 <span>Am</span> - 13:30 <span>Am</span></h4>
																	<h3 class="title">Lunch at Bebek Tepi Sawah Resto</h3>
																	
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse36" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading36">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
											<div class="panel panel-default lgx-panel">
												<div class="panel-heading" role="tab" id="heading37">
													<div class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse37" aria-expanded="true" aria-controls="collapseThree3">
															<div class="lgx-single-schedule">
																<div class="author">
																	<img src="assets/img/d1-9.png" alt="Speaker"/>
																</div>
																<div class="schedule-info">
																	<h4 class="time">13:30 <span>Am</span> - 15:30 <span>Am</span></h4>
																	<h3 class="title">Goes to Ngurah Rai Airport</h3>
																	
																</div>
															</div>
														</a>
													</div>
												</div>
												<div id="collapse37" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading37">
													<div class="panel-body">
														<p class="text">
															Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.
														</p>
														<h4 class="location"><strong>Location:</strong>  Hall 1, Building A , Golden Street , <span>Bali</span> </h4>
													</div>
												</div>
											</div>
										</div>

									</div>


									
								</div>
							</div>
						</div>
					</div>
					<!--//.ROW-->
					<div class="section-btn-area schedule-btn-area">
						<a class="lgx-btn lgx-btn-big" href="#"><span>Download Schedule (PDF)</span></a>
					</div>
                </div>
                <!-- //.CONTAINER -->
            </section>
            <!--SCHEDULE END-->
        </div>
    </main>




<!--FOOTER-->
<footer>
    <div id="lgx-footer" class="lgx-footer lgx-footer-black"> <!--lgx-footer-black-->
        <div class="lgx-inner-footer">
           
            <div class="container">
                <div class="lgx-footer-area">
                    <div class="lgx-footer-single">
                        <a class="logo" href="index.html"><img src="assets/img/footer-logo-its.png" alt="Logo"></a>
                    </div> <!--//footer-area-->
                    <div class="lgx-footer-single">
                        <h3 class="footer-title">Venue Location </h3>
                        <h4 class="date">
                            26-28 October, 2019
                        </h4>
                        <address>
                            Jakarta - Bali, Indonesia
                        </address>
                        <a id="myModalLabel2" data-toggle="modal" data-target="#lgx-modal-map" class="map-link" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> View Map location</a>
                    </div>
                    <div class="lgx-footer-single">
                        <h3 class="footer-title">Social Connection</h3>
                        <p class="text">
                            You should connect social area <br> for Any update
                        </p>
                        <ul class="list-inline lgx-social-footer">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- Modal-->
                <div id="lgx-modal-map" class="modal fade lgx-modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="lgxmapcanvas map-canvas-default" id="map_canvas"> </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- //.Modal-->

                <div class="lgx-footer-bottom">
                    <div class="lgx-copyright">
                         <p> <span>©</span> 2019 Intelligent Transport System is powered by <a href="#">PT Gamatechno Indonesia.</a></p>
                    </div>
                </div>

            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.footer Middle -->
    </div>
</footer>
<!--FOOTER END-->


</div>
<!--//.LGX SITE CONTAINER-->
<!-- *** ADD YOUR SITE SCRIPT HERE *** -->
<!-- JQUERY  -->
<script src="assets/js/vendor/jquery-1.12.4.min.js"></script>

<!-- BOOTSTRAP JS  -->
<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>

<!-- Smooth Scroll  -->
<script src="assets/libs/jquery.smooth-scroll.js"></script>

<!-- SKILLS SCRIPT  -->
<script src="assets/libs/jquery.validate.js"></script>

<!-- if load google maps then load this api, change api key as it may expire for limit cross as this is provided with any theme -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQvRGGtL6OrpP5xVMxq_0NgiMiRhm3ycI"></script>

<!-- CUSTOM GOOGLE MAP -->
<script type="text/javascript" src="assets/libs/gmap/jquery.googlemap.js"></script>

<!-- adding magnific popup js library -->
<script type="text/javascript" src="assets/libs/maginificpopup/jquery.magnific-popup.min.js"></script>

<!-- Owl Carousel  -->
<script src="assets/libs/owlcarousel/owl.carousel.min.js"></script>

<!-- COUNTDOWN   -->
<script src="assets/libs/countdown.js"></script>
<script src="assets/libs/timer/TimeCircles.js"></script>

<!-- Counter JS -->
<script src="assets/libs/waypoints.min.js"></script>
<script src="assets/libs/counterup/jquery.counterup.min.js"></script>

<!-- SMOTH SCROLL -->
<script src="assets/libs/jquery.smooth-scroll.min.js"></script>
<script src="assets/libs/jquery.easing.min.js"></script>

<!-- type js -->
<script src="assets/libs/typed/typed.min.js"></script>

<!-- header parallax js -->
<script src="assets/libs/header-parallax.js"></script>

<!-- instafeed js -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.1/instafeed.min.js"></script>-->
<script src="assets/libs/instafeed.min.js"></script>

<!-- CUSTOM SCRIPT  -->
<script src="assets/js/custom.script.js"></script>







</body>
</html>
