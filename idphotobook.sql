/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50535
Source Host           : localhost:3306
Source Database       : idphotobook

Target Server Type    : MYSQL
Target Server Version : 50535
File Encoding         : 65001

Date: 2020-06-27 02:22:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `event`
-- ----------------------------
DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `detail` text,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of event
-- ----------------------------
INSERT INTO `event` VALUES ('1', 'Flight from SIN to CGK 1', '01:53:06', '05:53:09', 'Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.', 'Hall 1, Building A , Golden Street , Jakarta');
INSERT INTO `event` VALUES ('2', 'Flight from SIN to CGK 2', '01:53:06', '05:53:09', 'Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.', 'Hall 1, Building A , Golden Street , Jakarta');
INSERT INTO `event` VALUES ('3', 'Flight from SIN to CGK 3', '01:53:06', '05:53:09', 'Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.', 'Hall 1, Building A , Golden Street , Jakarta');
INSERT INTO `event` VALUES ('4', 'Flight from SIN to CGK 4', '01:53:06', '05:53:09', 'Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.', 'Hall 1, Building A , Golden Street , Jakarta');
INSERT INTO `event` VALUES ('5', 'Flight from SIN to CGK 5', '01:53:06', '05:53:09', 'Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse flexitarian Truffaut synth art party deep v chillwave.', 'Hall 1, Building A , Golden Street , Jakarta');
