const path        			= require('path');
const express     		= require('express');
const bodyParser  	= require('body-parser');
const app         			= express();
const http        			= require('http').Server(app);

const config 				= require('./config/config');


var mainRoutesAPI = require(config.path.routeback);
app.use(mainRoutesAPI);

http.listen(config.server.port, () => {
  console.log(`Server is running ${config.server.url}:${config.server.port}`);
});

