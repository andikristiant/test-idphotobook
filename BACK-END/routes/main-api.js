var router 		= require('express').Router(),
	conn 		= require('../config/database');

//router
router.get('/api/event', getEventSchedule);

function getEventSchedule(req, res, next) {
	let sql = 'SELECT * FROM event';
	let query = conn.query(sql, function(err, results) {
			res.send(JSON.stringify({"status": 200, "rows": results}));
	});
}

module.exports = router;