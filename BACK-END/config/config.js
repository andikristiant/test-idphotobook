var main = {
	server:{
		url:"http://localhost",
		port:81
	},path:{
		routeback:"./routes/main-api"
	},
	printer:{
		path:'/dev/ttyUSB0',
		options:{
        	baudrate: 19200
    	}
	}
}
module.exports = main;