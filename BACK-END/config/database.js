//use mysql database
const mysql = require('mysql');

//Create Connection
const conn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '1',
  database: 'idphotobook'
});

//connect to database
conn.connect((err) =>{
  if(err) throw err;
  console.log('Database Connected...');
});

module.exports = conn